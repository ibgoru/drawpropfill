﻿namespace DrawPropFill
{
	partial class ProcessForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing&&( components!=null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProcessForm));
			this.label1 = new System.Windows.Forms.Label();
			this.FolderPath = new System.Windows.Forms.TextBox();
			this.FolderSelect = new System.Windows.Forms.Button();
			this.OkBtn = new System.Windows.Forms.Button();
			this.CancelBtn = new System.Windows.Forms.Button();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.SelectAll = new System.Windows.Forms.ToolStripButton();
			this.SelectUnequal = new System.Windows.Forms.ToolStripButton();
			this.SelectEmpty = new System.Windows.Forms.ToolStripButton();
			this.DeselectAll = new System.Windows.Forms.ToolStripButton();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.ProgressBar = new System.Windows.Forms.ToolStripProgressBar();
			this.FoundDrawings = new System.Windows.Forms.ToolStripStatusLabel();
			this.Drawing = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.DrawingPartId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.DrawingPartName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.Model = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.ModelPartId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.ModelPartName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.DrawingsList = new System.Windows.Forms.ListView();
			this.CfgPartId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.CfgPartName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.Error = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.linkLabel1 = new System.Windows.Forms.LinkLabel();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.CfgSelectRadio = new System.Windows.Forms.RadioButton();
			this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
			this.DrawingsPartIdEdit = new System.Windows.Forms.ToolStripTextBox();
			this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
			this.DrawingsPartNameEdit = new System.Windows.Forms.ToolStripTextBox();
			this.DrawingsListContext = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.ClipboardBuffer = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
			this.PasteFromBuffer = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStrip1.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			this.DrawingsListContext.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(10, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(42, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Папка:";
			// 
			// FolderPath
			// 
			this.FolderPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.FolderPath.Location = new System.Drawing.Point(57, 12);
			this.FolderPath.Name = "FolderPath";
			this.FolderPath.ReadOnly = true;
			this.FolderPath.Size = new System.Drawing.Size(618, 20);
			this.FolderPath.TabIndex = 1;
			// 
			// FolderSelect
			// 
			this.FolderSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.FolderSelect.Location = new System.Drawing.Point(682, 10);
			this.FolderSelect.Name = "FolderSelect";
			this.FolderSelect.Size = new System.Drawing.Size(83, 23);
			this.FolderSelect.TabIndex = 2;
			this.FolderSelect.Text = "Выбрать...";
			this.FolderSelect.UseVisualStyleBackColor = true;
			this.FolderSelect.Click += new System.EventHandler(this.FolderSelect_Click);
			// 
			// OkBtn
			// 
			this.OkBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.OkBtn.Location = new System.Drawing.Point(562, 425);
			this.OkBtn.Name = "OkBtn";
			this.OkBtn.Size = new System.Drawing.Size(99, 27);
			this.OkBtn.TabIndex = 4;
			this.OkBtn.Text = "Обработка";
			this.OkBtn.UseVisualStyleBackColor = true;
			this.OkBtn.Click += new System.EventHandler(this.OkBtn_Click);
			// 
			// CancelBtn
			// 
			this.CancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.CancelBtn.Location = new System.Drawing.Point(667, 425);
			this.CancelBtn.Name = "CancelBtn";
			this.CancelBtn.Size = new System.Drawing.Size(99, 27);
			this.CancelBtn.TabIndex = 5;
			this.CancelBtn.Text = "Отмена";
			this.CancelBtn.UseVisualStyleBackColor = true;
			// 
			// toolStrip1
			// 
			this.toolStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.toolStrip1.AutoSize = false;
			this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SelectAll,
            this.SelectUnequal,
            this.SelectEmpty,
            this.DeselectAll,
            this.toolStripLabel1,
            this.DrawingsPartIdEdit,
            this.toolStripLabel2,
            this.DrawingsPartNameEdit});
			this.toolStrip1.Location = new System.Drawing.Point(13, 45);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(752, 28);
			this.toolStrip1.TabIndex = 6;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// SelectAll
			// 
			this.SelectAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.SelectAll.Image = ((System.Drawing.Image)(resources.GetObject("SelectAll.Image")));
			this.SelectAll.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.SelectAll.Name = "SelectAll";
			this.SelectAll.Size = new System.Drawing.Size(23, 25);
			this.SelectAll.Text = "Отметить все";
			this.SelectAll.Click += new System.EventHandler(this.SelectAll_Click);
			// 
			// SelectUnequal
			// 
			this.SelectUnequal.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.SelectUnequal.Image = ((System.Drawing.Image)(resources.GetObject("SelectUnequal.Image")));
			this.SelectUnequal.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.SelectUnequal.Name = "SelectUnequal";
			this.SelectUnequal.Size = new System.Drawing.Size(23, 25);
			this.SelectUnequal.Text = "Отметить несовпадающие";
			this.SelectUnequal.Click += new System.EventHandler(this.SelectUnequal_Click);
			// 
			// SelectEmpty
			// 
			this.SelectEmpty.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.SelectEmpty.Image = ((System.Drawing.Image)(resources.GetObject("SelectEmpty.Image")));
			this.SelectEmpty.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.SelectEmpty.Name = "SelectEmpty";
			this.SelectEmpty.Size = new System.Drawing.Size(23, 25);
			this.SelectEmpty.Text = "Отметить незаполненые";
			this.SelectEmpty.Click += new System.EventHandler(this.SelectEmpty_Click);
			// 
			// DeselectAll
			// 
			this.DeselectAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.DeselectAll.Image = ((System.Drawing.Image)(resources.GetObject("DeselectAll.Image")));
			this.DeselectAll.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.DeselectAll.Name = "DeselectAll";
			this.DeselectAll.Size = new System.Drawing.Size(23, 25);
			this.DeselectAll.Text = "Снять отметки";
			this.DeselectAll.Click += new System.EventHandler(this.DeselectAll_Click);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProgressBar,
            this.FoundDrawings});
			this.statusStrip1.Location = new System.Drawing.Point(0, 459);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(778, 22);
			this.statusStrip1.TabIndex = 7;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// ProgressBar
			// 
			this.ProgressBar.Name = "ProgressBar";
			this.ProgressBar.Size = new System.Drawing.Size(100, 16);
			// 
			// FoundDrawings
			// 
			this.FoundDrawings.Name = "FoundDrawings";
			this.FoundDrawings.Size = new System.Drawing.Size(67, 17);
			this.FoundDrawings.Text = "Найдено: 0";
			// 
			// Drawing
			// 
			this.Drawing.Text = "Чертеж";
			this.Drawing.Width = 139;
			// 
			// DrawingPartId
			// 
			this.DrawingPartId.Text = "Обозначение";
			this.DrawingPartId.Width = 100;
			// 
			// DrawingPartName
			// 
			this.DrawingPartName.Text = "Наименование";
			this.DrawingPartName.Width = 100;
			// 
			// Model
			// 
			this.Model.Text = "Модель";
			this.Model.Width = 151;
			// 
			// ModelPartId
			// 
			this.ModelPartId.Text = "Обозначение (модель)";
			this.ModelPartId.Width = 100;
			// 
			// ModelPartName
			// 
			this.ModelPartName.Text = "Наименование (модель)";
			this.ModelPartName.Width = 100;
			// 
			// DrawingsList
			// 
			this.DrawingsList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.DrawingsList.CheckBoxes = true;
			this.DrawingsList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Drawing,
            this.DrawingPartId,
            this.DrawingPartName,
            this.Model,
            this.ModelPartId,
            this.ModelPartName,
            this.CfgPartId,
            this.CfgPartName,
            this.Error});
			this.DrawingsList.ContextMenuStrip = this.DrawingsListContext;
			this.DrawingsList.FullRowSelect = true;
			this.DrawingsList.GridLines = true;
			this.DrawingsList.HideSelection = false;
			this.DrawingsList.Location = new System.Drawing.Point(13, 76);
			this.DrawingsList.Name = "DrawingsList";
			this.DrawingsList.Size = new System.Drawing.Size(752, 343);
			this.DrawingsList.TabIndex = 3;
			this.DrawingsList.UseCompatibleStateImageBehavior = false;
			this.DrawingsList.View = System.Windows.Forms.View.Details;
			this.DrawingsList.ItemActivate += new System.EventHandler(this.DrawingsList_ItemActivate);
			this.DrawingsList.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.DrawingsList_ItemCheck);
			this.DrawingsList.SelectedIndexChanged += new System.EventHandler(this.DrawingsList_SelectedIndexChanged);
			this.DrawingsList.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DrawingsList_MouseClick);
			// 
			// CfgPartId
			// 
			this.CfgPartId.Text = "Обозначение (конф.)";
			this.CfgPartId.Width = 100;
			// 
			// CfgPartName
			// 
			this.CfgPartName.Text = "Наименование (конф.)";
			this.CfgPartName.Width = 100;
			// 
			// Error
			// 
			this.Error.Text = "Ошибка";
			this.Error.Width = 150;
			// 
			// linkLabel1
			// 
			this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.linkLabel1.AutoSize = true;
			this.linkLabel1.Location = new System.Drawing.Point(143, 432);
			this.linkLabel1.Name = "linkLabel1";
			this.linkLabel1.Size = new System.Drawing.Size(264, 13);
			this.linkLabel1.TabIndex = 8;
			this.linkLabel1.TabStop = true;
			this.linkLabel1.Text = "Почему я не могу отметить некоторые элементы?";
			this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
			// 
			// radioButton1
			// 
			this.radioButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.radioButton1.AutoSize = true;
			this.radioButton1.Location = new System.Drawing.Point(15, 421);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(96, 17);
			this.radioButton1.TabIndex = 9;
			this.radioButton1.Text = "Из документа";
			this.radioButton1.UseVisualStyleBackColor = true;
			// 
			// CfgSelectRadio
			// 
			this.CfgSelectRadio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.CfgSelectRadio.AutoSize = true;
			this.CfgSelectRadio.Checked = true;
			this.CfgSelectRadio.Location = new System.Drawing.Point(15, 439);
			this.CfgSelectRadio.Name = "CfgSelectRadio";
			this.CfgSelectRadio.Size = new System.Drawing.Size(114, 17);
			this.CfgSelectRadio.TabIndex = 10;
			this.CfgSelectRadio.TabStop = true;
			this.CfgSelectRadio.Text = "Из конфигурации";
			this.CfgSelectRadio.UseVisualStyleBackColor = true;
			// 
			// toolStripLabel1
			// 
			this.toolStripLabel1.Name = "toolStripLabel1";
			this.toolStripLabel1.Size = new System.Drawing.Size(84, 25);
			this.toolStripLabel1.Text = "Обозначение:";
			// 
			// DrawingsPartIdEdit
			// 
			this.DrawingsPartIdEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.DrawingsPartIdEdit.Enabled = false;
			this.DrawingsPartIdEdit.Name = "DrawingsPartIdEdit";
			this.DrawingsPartIdEdit.Size = new System.Drawing.Size(150, 28);
			this.DrawingsPartIdEdit.Leave += new System.EventHandler(this.DrawingsList_SelectedIndexChanged);
			// 
			// toolStripLabel2
			// 
			this.toolStripLabel2.Name = "toolStripLabel2";
			this.toolStripLabel2.Size = new System.Drawing.Size(93, 25);
			this.toolStripLabel2.Text = "Наименование:";
			// 
			// DrawingsPartNameEdit
			// 
			this.DrawingsPartNameEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.DrawingsPartNameEdit.Enabled = false;
			this.DrawingsPartNameEdit.Name = "DrawingsPartNameEdit";
			this.DrawingsPartNameEdit.Size = new System.Drawing.Size(150, 28);
			this.DrawingsPartNameEdit.Leave += new System.EventHandler(this.DrawingsList_SelectedIndexChanged);
			// 
			// DrawingsListContext
			// 
			this.DrawingsListContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ClipboardBuffer,
            this.toolStripMenuItem1,
            this.PasteFromBuffer});
			this.DrawingsListContext.Name = "DrawingsListContext";
			this.DrawingsListContext.Size = new System.Drawing.Size(187, 54);
			// 
			// ClipboardBuffer
			// 
			this.ClipboardBuffer.Name = "ClipboardBuffer";
			this.ClipboardBuffer.Size = new System.Drawing.Size(186, 22);
			this.ClipboardBuffer.Text = "Копировать в буфер";
			this.ClipboardBuffer.Click += new System.EventHandler(this.ClipboardBuffer_Click);
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(183, 6);
			// 
			// PasteFromBuffer
			// 
			this.PasteFromBuffer.Name = "PasteFromBuffer";
			this.PasteFromBuffer.Size = new System.Drawing.Size(186, 22);
			this.PasteFromBuffer.Text = "Вставить из буфера";
			this.PasteFromBuffer.Click += new System.EventHandler(this.PasteFromBuffer_Click);
			// 
			// ProcessForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(778, 481);
			this.Controls.Add(this.CfgSelectRadio);
			this.Controls.Add(this.radioButton1);
			this.Controls.Add(this.linkLabel1);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.toolStrip1);
			this.Controls.Add(this.CancelBtn);
			this.Controls.Add(this.OkBtn);
			this.Controls.Add(this.DrawingsList);
			this.Controls.Add(this.FolderSelect);
			this.Controls.Add(this.FolderPath);
			this.Controls.Add(this.label1);
			this.Name = "ProcessForm";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Обработка чертежей";
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.DrawingsListContext.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox FolderPath;
		private System.Windows.Forms.Button FolderSelect;
		private System.Windows.Forms.Button OkBtn;
		private System.Windows.Forms.Button CancelBtn;
		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ToolStripButton SelectAll;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripProgressBar ProgressBar;
		private System.Windows.Forms.ToolStripStatusLabel FoundDrawings;
		private System.Windows.Forms.ColumnHeader Drawing;
		private System.Windows.Forms.ColumnHeader DrawingPartId;
		private System.Windows.Forms.ColumnHeader DrawingPartName;
		private System.Windows.Forms.ColumnHeader Model;
		private System.Windows.Forms.ColumnHeader ModelPartId;
		private System.Windows.Forms.ColumnHeader ModelPartName;
		private System.Windows.Forms.ListView DrawingsList;
		private System.Windows.Forms.ToolStripButton SelectEmpty;
		private System.Windows.Forms.ToolStripButton DeselectAll;
		private System.Windows.Forms.ToolStripButton SelectUnequal;
		private System.Windows.Forms.ColumnHeader Error;
		private System.Windows.Forms.LinkLabel linkLabel1;
		private System.Windows.Forms.ColumnHeader CfgPartId;
		private System.Windows.Forms.ColumnHeader CfgPartName;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.RadioButton CfgSelectRadio;
		private System.Windows.Forms.ToolStripLabel toolStripLabel1;
		private System.Windows.Forms.ToolStripTextBox DrawingsPartIdEdit;
		private System.Windows.Forms.ToolStripLabel toolStripLabel2;
		private System.Windows.Forms.ToolStripTextBox DrawingsPartNameEdit;
		private System.Windows.Forms.ContextMenuStrip DrawingsListContext;
		private System.Windows.Forms.ToolStripMenuItem ClipboardBuffer;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem PasteFromBuffer;
	}
}