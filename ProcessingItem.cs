﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawPropFill
{
	class ProcessingItem
	{
		public enum SOURCE_TYPE { MODEL, CONFIG };

		public ProcessingItem()
		{
			m_source_drw=m_source_mdl=m_cfg_partname=m_cfg_partid=m_draw_partid=m_draw_partname=m_mdl_partid=m_mdl_partname=m_error="";
			m_was_changed=false;
		}

		private string m_source_drw;
		public string SourceDrawing
		{
			get { return m_source_drw; }
			set { m_source_drw=value; }
		}

		private string m_source_mdl;
		public string SourceModel
		{
			get { return m_source_mdl; }
			set { m_source_mdl=value; }
		}

		private string m_cfg_partid;
		public string ConfigPartId {
				get { return m_cfg_partid; }
				set { m_cfg_partid=value; }
		}

		private bool m_was_changed;
	
		public bool WasChanged 
		{
				get { return m_was_changed; }
				set { m_was_changed=value; }
		}


		private string m_cfg_partname;
		public string ConfigPartName {
				get { return m_cfg_partname; }
				set { m_cfg_partname=value; }
		}

		private string m_draw_partid;
		public string DrawingPartId
		{
			get { return m_draw_partid; }
			set { m_draw_partid=value; }
		}
		private string m_draw_partname;
		public string DrawingPartName
		{
			get { return m_draw_partname; }
			set { m_draw_partname=value; }
		}

		private string m_mdl_partid;
		public string ModelPartId
		{
			get { return m_mdl_partid; }
			set { m_mdl_partid=value; }
		}
		private string m_mdl_partname;
		public string ModelPartName
		{
			get { return m_mdl_partname; }
			set { m_mdl_partname=value; }
		}
		private string m_error;
		public string Error
		{
			get { return m_error; }
			set { m_error=value; }
		}

	}
}
