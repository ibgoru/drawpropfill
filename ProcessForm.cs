﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawPropFill
{








	public partial class ProcessForm: Form
	{
		private ListViewItem m_item=null;	
		int column_index = 1;
		public ProcessForm()
		{
			InitializeComponent();
	
		}


		void UpdateList()
		{
			foreach( ListViewItem item in DrawingsList.Items )
			{
				ProcessingItem pitem = (ProcessingItem)item.Tag;

				item.SubItems[1].Text = pitem.DrawingPartId;
				item.SubItems[2].Text = pitem.DrawingPartName;
			}
		}
		void FillListWithItems( List<ProcessingItem> items )
		{
			DrawingsList.Items.Clear();
			ProgressBar.Minimum=ProgressBar.Value=0;
			ProgressBar.Maximum=items.Count;
			ProgressBar.Step=1;
			foreach( var item in items )
			{
				var list_item = DrawingsList.Items.Add( Path.GetFileName(item.SourceDrawing) );
				list_item.Tag = item;

				list_item.SubItems.Add( item.DrawingPartId );
				list_item.SubItems.Add( item.DrawingPartName );

				
				list_item.SubItems.Add( Path.GetFileName(item.SourceModel) );

				list_item.SubItems.Add( item.ModelPartId );
				list_item.SubItems.Add( item.ModelPartName );

				list_item.SubItems.Add( item.ConfigPartId );
				list_item.SubItems.Add( item.ConfigPartName );

				list_item.SubItems.Add( item.Error );

				
				ProgressBar.PerformStep();
			}
			ProgressBar.Value=0;
		}

		private void FolderSelect_Click( object sender, EventArgs e )
		{
			var open_folder = new FolderBrowserDialog();
			open_folder.Description="Выберите папку с чертежами";
			open_folder.SelectedPath=Properties.Settings.Default.SEARCHPATH;
			if( open_folder.ShowDialog()==DialogResult.OK )
			{
				FolderPath.Text=open_folder.SelectedPath;
				Properties.Settings.Default.SEARCHPATH = FolderPath.Text;

				var items_list = new List<ProcessingItem>();

				Program.SeekingForDrawings(items_list, open_folder.SelectedPath );

				FillListWithItems( items_list );

				FoundDrawings.Text="Найдено:"+items_list.Count.ToString();
			}
		}

		private void SelectAll_Click( object sender, EventArgs e )
		{
			foreach( ListViewItem itm in DrawingsList.Items ) itm.Checked=true;
		}

		private void DeselectAll_Click( object sender, EventArgs e )
		{
			foreach( ListViewItem itm in DrawingsList.Items ) itm.Checked=false;
		}

		private void SelectEmpty_Click( object sender, EventArgs e )
		{
			foreach( ListViewItem itm in DrawingsList.Items ) 
			{
				ProcessingItem pitem = (ProcessingItem)itm.Tag;
				itm.Checked=( pitem.DrawingPartId.Trim().Length == 0 || pitem.DrawingPartName.Trim().Length == 0 );
			}
		}

		private void OkBtn_Click( object sender, EventArgs e )
		{
			if( MessageBox.Show("Начать заполнение свойств для выделенных чертежей?", "Вопрос",  MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes )
			{
				

				ProgressBar.Minimum=ProgressBar.Value=0;
				ProgressBar.Maximum=DrawingsList.CheckedItems.Count;
				foreach( ListViewItem itm in DrawingsList.CheckedItems ) 
				{
					ProcessingItem pitem = (ProcessingItem)itm.Tag;
					Program.PutPropertiesIntoDrawings( pitem, CfgSelectRadio.Checked );
					ProgressBar.PerformStep();
				}
				UpdateList();

				ProgressBar.Value=0;
			}
		}

		private void SelectUnequal_Click( object sender, EventArgs e )
		{
			foreach( ListViewItem itm in DrawingsList.Items ) 
			{
				ProcessingItem pitem = (ProcessingItem)itm.Tag;
				itm.Checked=( pitem.DrawingPartId.Trim() != pitem.ModelPartId.Trim() || pitem.DrawingPartName.Trim() != pitem.ModelPartName.Trim() );
			}
		}

		private void DrawingsList_ItemCheck( object sender, ItemCheckEventArgs e )
		{
			ProcessingItem pitem = (ProcessingItem)DrawingsList.Items[e.Index].Tag;
			if( pitem.Error.Length!=0 ) {
				e.NewValue=CheckState.Unchecked;
			}
		}

		private void linkLabel1_LinkClicked( object sender, LinkLabelLinkClickedEventArgs e )
		{
			MessageBox.Show("Элемент не может быть отмечен если произошла ошибка. Наиболее частыми причинами ошибки является неподходящая версия SolidWorks, недоступный для записи файл чертежа, файл чертежа или модели занят программой SolidWorks. Смотрите текст ошибки в поле \"Ошибка\"");
		}

		private void DrawingsList_MouseClick( object sender, MouseEventArgs e )
		{
			ListViewHitTestInfo hit = DrawingsList.HitTest(e.Location);
			if( hit.SubItem!=null )
				column_index = hit.Item.SubItems.IndexOf(hit.SubItem);
		}

		private void DrawingsList_ItemActivate( object sender, EventArgs e )
		{
			
		}

		private void DrawingsList_SelectedIndexChanged( object sender, EventArgs e )
		{
			if( m_item==null )
				DrawingsPartNameEdit.Enabled=DrawingsPartIdEdit.Enabled=true;
			else
			{

				ProcessingItem pitem = (ProcessingItem)m_item.Tag;

				pitem.DrawingPartId= DrawingsPartIdEdit.Text;
				pitem.DrawingPartName = DrawingsPartNameEdit.Text;
				pitem.WasChanged=true;

				m_item.SubItems[1].Text = pitem.DrawingPartId;
				m_item.SubItems[2].Text = pitem.DrawingPartName;

			}
			if( DrawingsList.SelectedItems.Count>0 )
			{
				m_item = DrawingsList.SelectedItems[0];
				ProcessingItem pitem = (ProcessingItem)m_item.Tag;
				DrawingsPartIdEdit.Text = pitem.DrawingPartId;
				DrawingsPartNameEdit.Text = pitem.DrawingPartName;
			}

				
		}

		private void ClipboardBuffer_Click( object sender, EventArgs e )
		{
			if( DrawingsList.SelectedItems.Count>0 )
			{
				string text_to_insert=DrawingsList.SelectedItems[0].SubItems[column_index].Text;
				if( text_to_insert!=null && text_to_insert.Length>0 )
					Clipboard.SetText( text_to_insert );
			}
		}

		private void PasteFromBuffer_Click( object sender, EventArgs e )
		{
			if( (column_index==1 || column_index==2) && DrawingsList.SelectedItems.Count>0 && m_item!=null)
			{
				m_item = DrawingsList.SelectedItems[0];
				ProcessingItem pitem = (ProcessingItem)m_item.Tag;
				DrawingsList.SelectedItems[0].SubItems[column_index].Text=Clipboard.GetText();
				if( column_index==1 )
					DrawingsPartIdEdit.Text = DrawingsList.SelectedItems[0].SubItems[column_index].Text;
				else if( column_index==2 )
					DrawingsPartNameEdit.Text = DrawingsList.SelectedItems[0].SubItems[column_index].Text;
			}
		}
	}
}
