﻿using SolidWorks.Interop.swdocumentmgr;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DrawPropFill
{
	class Program
	{
#if SW2018 
	// SolidWorks 2018 document manager key
	public const string swdm_key = "APPIUS:swdocmgr_general-11785-02051-00064-50177-08535-34307-00007-11832-48199-05468-24614-07618-20073-03620-26630-41251-03703-35294-27964-46990-01743-22923-16644-09537-19797-14337-27744-51290-50890-25690-25184-1104,swdocmgr_previews-11785-02051-00064-50177-08535-34307-00007-23440-53286-04374-53190-17388-23845-62792-30727-34306-46808-11212-36064-18481-30419-22927-16644-09537-19797-14337-27744-51290-50890-25690-25184-1104";
#elif SW2017
	// SolidWorks 2017 document manager key
	public const string swdm_key = "APPIUS:swdocmgr_general-11785-02051-00064-33793-08504-34307-00007-00544-17323-08416-48842-33607-50757-22378-04103-63211-36220-14210-15536-41837-38838-24422-16644-09537-19797-14337-25184-55898-58562-25690-25184-1105,swdocmgr_previews-11785-02051-00064-33793-08504-34307-00007-57512-21302-20455-54596-27187-24422-45333-04097-31105-21530-05746-16662-16497-52660-23159-16644-09537-19797-14337-25184-55898-58562-25690-25184-1107";
#elif SW2016
	// SolidWorks 2016 document manager key
	public const string swdm_key = "APPIUS:swdocmgr_general-11785-02051-00064-17409-08473-34307-00007-38336-56727-14146-56823-20188-38805-65081-10247-26516-32163-01728-08791-10341-42402-23028-16644-09537-19797-14337-29284-52314-50378-25690-25184-1088,swdocmgr_previews-11785-02051-00064-17409-08473-34307-00007-44888-58523-02810-65263-32880-05940-50442-63488-50773-62693-08075-49633-45282-54150-22987-16644-09537-19797-14337-29284-52314-50378-25690-25184-1085";
#elif SW2015
	// SolidWorks 2015 document manager key
	public const string swdm_key = "APPIUS:swdocmgr_general-11785-02051-00064-01025-08442-34307-00007-29280-61880-60408-36339-46607-13199-33116-43008-25099-00675-01937-41673-55861-25514-23848-16644-09537-19797-14337-28768-58970-57546-25690-25184-1064,swdocmgr_previews-11785-02051-00064-01025-08442-34307-00007-12904-44408-37514-02370-58183-44184-56728-45062-47327-34773-64550-41070-17838-42708-24362-16644-09537-19797-14337-28768-58970-57546-25690-25184-1065";
#else
	// SolidWorks 2014 document manager key 
	public const string swdm_key = "5638C832AAC82A484CAB061A32A659134E38BFDB6B45AA3C";
#endif	

	public static SwDMApplication4 m_swdmapp=null;
	static SwDmDocumentType GetDocumentType( string drawing_fullpath )
	{
		string file_extention=Path.GetExtension( drawing_fullpath ).ToLower();
		if( file_extention.Contains( "sldprt") ) return SwDmDocumentType.swDmDocumentPart;
		else if ( file_extention.Contains( "sldasm") ) return SwDmDocumentType.swDmDocumentAssembly;
		else if ( file_extention.Contains( "slddrw ") ) return SwDmDocumentType.swDmDocumentDrawing;
		else return SwDmDocumentType.swDmDocumentUnknown;	
	}


	static string PrintOpenError( SwDmDocumentOpenError err )
	{
		switch( err )
		{
			case SwDmDocumentOpenError.swDmDocumentOpenErrorFail: return "Ошибка!";
			case SwDmDocumentOpenError.swDmDocumentOpenErrorFileNotFound: return "Файл не найден!";
			case SwDmDocumentOpenError.swDmDocumentOpenErrorFileReadOnly: return "Файл только для чтения!";	
			case SwDmDocumentOpenError.swDmDocumentOpenErrorFutureVersion: return "Файл будущей версии!";	
			case SwDmDocumentOpenError.swDmDocumentOpenErrorNoLicense: return "Нет лицензии! Версия SolidWorks не подходит!";	
			case SwDmDocumentOpenError.swDmDocumentOpenErrorNonSW: return "Формат файла не SolidWorks";	
		}
		return "";
	}




	public static string ResolveMacro( SwDMDocument18 mdl, string value )
	{
			Regex _regex=new Regex("\\$PRP:\"SW-[a-zA-Z -]+\"");
			var match = _regex.Match(value);
			if (match.Success)
			{
				int index=match.Groups[0].Index;
				int length=match.Groups[0].Length;
				string replace_value="";
				switch( match.Groups[0].Value.ToLower() )
				{
					case "$prp:\"sw-title\"": replace_value=mdl.Title; break;
					case "$prp:\"sw-file name\"": replace_value=Path.GetFileNameWithoutExtension(mdl.FullName); break;
				}
				return value.Replace( match.Groups[0].Value, replace_value );
			}
		return value;
	}

	public static void PutPropertiesIntoDrawings( ProcessingItem pit, bool cfg )
	{
		string error;
		SwDMDocument18 drawing = OpenSolidDoc( pit.SourceDrawing, out error );
		if( drawing!=null )
		{
			try {
				if( !pit.WasChanged )
					pit.DrawingPartId = cfg?pit.ConfigPartId:pit.ModelPartId;
				drawing.SetCustomProperty("Обозначение", pit.DrawingPartId );
			} catch( Exception e )
			{
				try {
					drawing.AddCustomProperty("Обозначение",SwDmCustomInfoType.swDmCustomInfoText, pit.DrawingPartId );
				}
				catch(Exception add_exception)
				{
					pit.Error="Ошибка при добавлении свойства Обозначение!";
				}

				e.ToString();
			}
			try {
				if( !pit.WasChanged )
					pit.DrawingPartName = cfg?pit.ConfigPartName:pit.ModelPartName;
				drawing.SetCustomProperty("Наименование", pit.DrawingPartName );
			} catch( Exception e )
			{
				try {
					drawing.AddCustomProperty("Наименование",SwDmCustomInfoType.swDmCustomInfoText, pit.DrawingPartName  );
				}
				catch(Exception add_exception)
				{
					pit.Error="Ошибка при добавлении свойства Наименование!";
				}

				e.ToString();
			}

			drawing.Save();
			drawing.CloseDoc();
		}
		else
			pit.Error=error;
	}

	public static bool FindProp( SwDMDocument18 doc , string name )
	{
		object names, values, linked, types;
		doc.GetAllCustomPropertyNamesAndValues( out names, out types, out linked, out values);
		string[] _names = (string[])names;
		if( _names!=null )
			foreach( string _name in _names )
				if( String.Compare( _name, name, true )== 0 )
					return true;
		return false;
	}

	public static void SeekingForDrawings( List<ProcessingItem> items_list,  string path )
	{
		string[] files = Directory.GetFiles( path, "*.slddrw" );

		foreach( var file in files )
		{
			// Pass temp
			if( file.Contains("~$") ) continue;

			ProcessingItem new_item = new ProcessingItem();

			string correspond_mdl_path = Path.GetDirectoryName( file )+"\\"+Path.GetFileNameWithoutExtension(file) + ".sldprt";
			if( !File.Exists(correspond_mdl_path) )
				correspond_mdl_path = Path.GetDirectoryName( file )+"\\"+Path.GetFileNameWithoutExtension(file) + ".sldasm";

			new_item.SourceDrawing = file;

			if( File.Exists(correspond_mdl_path) ) 
			{
				new_item.SourceModel = correspond_mdl_path;
				string error;
				SwDMDocument18 drawing = OpenSolidDoc(  file, out error );
				if( drawing!=null )
				{
					SwDmCustomInfoType prop_type;
					new_item.DrawingPartId = drawing.GetCustomProperty2( "Обозначение", out prop_type );
					new_item.DrawingPartName = drawing.GetCustomProperty2( "Наименование", out prop_type );
					drawing.CloseDoc();
				} else new_item.Error = error;
				SwDMDocument18 mdl = OpenSolidDoc(  new_item.SourceModel, out error, true );
				if( mdl!=null )
				{
					string active_cfg = mdl.ConfigurationManager.GetActiveConfigurationName();
					SwDMConfiguration12 cfg =  (SwDMConfiguration12)mdl.ConfigurationManager.GetConfigurationByName(active_cfg);

					SwDmCustomInfoType prop_type;
					if( FindProp( mdl, "Обозначение") )
						new_item.ModelPartId = ResolveMacro( mdl, mdl.GetCustomProperty( "Обозначение", out prop_type ));
					if( FindProp( mdl, "Наименование") )
						new_item.ModelPartName = ResolveMacro( mdl,mdl.GetCustomProperty( "Наименование", out prop_type ) );

					string[] names = cfg.GetCustomPropertyNames();
					if( names!=null ) 
					{
						for(int i=0;i<names.Length;i++) names[i]=names[i].ToLower();
						if( names.Contains("обозначение") )
							new_item.ConfigPartId = ResolveMacro( mdl, cfg.GetCustomProperty( "Обозначение", out prop_type) );
						if( names.Contains("наименование") )
							new_item.ConfigPartName = ResolveMacro( mdl, cfg.GetCustomProperty( "Наименование", out prop_type) );
					}
					mdl.CloseDoc();
				}
			}
			items_list.Add( new_item );
		}
		;
		foreach( var directory in Directory.GetDirectories( path ) )
			SeekingForDrawings( items_list, directory );
	}


	static SwDMDocument18 OpenSolidDoc( string fullpath, out string error, bool read_only=false )
	{
		error="";

		SwDmDocumentType mdl_type=SwDmDocumentType.swDmDocumentUnknown;
		Debug.Assert(m_swdmapp!=null);
		
		SwDmDocumentOpenError open_error;
		SwDMDocument18 mdl_doc=(SwDMDocument18)m_swdmapp.GetDocument( fullpath, mdl_type, read_only, out open_error );
		if( mdl_doc!=null && open_error==SwDmDocumentOpenError.swDmDocumentOpenErrorNone)
		{
			return mdl_doc;
		}
		else
		{
			error=PrintOpenError( open_error );
		}
		return null;
	}


		[STAThread]
		static void Main( string[] args )
			{
			var swClassFact = new SwDMClassFactory();
			m_swdmapp = (SwDMApplication4)swClassFact.GetApplication(swdm_key);
			if( m_swdmapp==null )
			{
				MessageBox.Show("Невозможно создать объект менеджера управления документами. Проверьте версию SolidWorks!");
				return;
			}

			
			ProcessForm frm = new ProcessForm();
			frm.ShowDialog();


		}
	}
}
